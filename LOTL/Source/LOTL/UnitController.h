#ifndef UNIT_H
#define UNIT_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class UnitController :public Message
{
private:
	FString URL = Message::IP + "/unit_buy/?";
	FString EXTENSION = "/unit_buy/?";
	FHttpModule* Http;
public:
	UnitController(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> CreateUnit(float sessionID, FString UnitType, FString UnitSize, FString UnitOwner, FString X, FString Y);
	void RefreshIP();
	~UnitController();
};
#endif