#include "LOTL.h"
#include "ChatController.h"


ChatController::ChatController(int year, int month, int day)
{

}

TSharedRef<IHttpRequest> ChatController::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//String serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> ChatController::CreateChat(float sessionID, float playerID, FString Message){
	//Create Object to send 
	RefreshIP();
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());

	JsonObject->SetStringField(TEXT("Session_id"), FString::FromInt(sessionID));
	JsonObject->SetStringField(TEXT("Sender_id"), FString::FromInt(playerID));
	JsonObject->SetStringField(TEXT("Message"), Message);
	return(CreatePostSend(JsonObject, URL));
}
void ChatController::RefreshIP(){
	URL = IP + EXTENSION;
}
ChatController::~ChatController()
{
}
TSharedRef<IHttpRequest>  ChatController::AddSessionChatRequestContent(TSharedRef<IHttpRequest> Request, float sessionID, FString lastCall){
	//add parameters to the URL
	RefreshIP();
	
	URL = URL + "Session_id=" + FString::FromInt(sessionID) + "&" + "Time=" + lastCall;
	//This is the url on which to process the request
	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, URL);
	//UE_LOG(YourLog, Warning, TEXT(URL));
	Request->SetURL(URL);
	return Request;
}
