#ifndef CHARACTER_GROUP_H
#define CHARACTER_GROUP_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class SessionCharacterGroup : public Message
{
private:
	FString URL = Message::IP + "/session-player-group/?";
	FString EXTENSION = "/session-player-group/?";
	FHttpModule* Http;
public:
	SessionCharacterGroup(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request, int sessionID);
	TSharedRef<IHttpRequest> ChooseFaction(FString faction, float playerID, float sessionID);
	TSharedRef<IHttpRequest> GetTerritoryMap(float playerID, float sessionID);
	void RefreshIP();
};

#endif