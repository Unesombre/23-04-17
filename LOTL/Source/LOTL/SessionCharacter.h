#ifndef CHARACTER_H
#define CHARACTER_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class SessionCharacter : public Message
{
private:
	FString URL = Message::IP + "/session-player/?";
	FString EXTENSION = "/session-player/?";
	FHttpModule* Http;
public:
	SessionCharacter(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request, int sessionID,int playerID);
	TSharedRef<IHttpRequest> ChooseFaction(FString faction, float playerID, float sessionID);
	TSharedRef<IHttpRequest> GetTerritoryMap(float playerID, float sessionID);
	void RefreshIP();
};

#endif