#include "LOTL.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#pragma once

class Message
{
public:
	FString IP = "http://10.154.160.248:8000";
	Message();
	TSharedRef<IHttpRequest> CreatePostSend(TSharedPtr<FJsonObject> JsonObject,FString URL);
	void AddGetHeaders(TSharedRef<IHttpRequest> Request);
	void SetNewIp(FString newIP);
	~Message();
};

