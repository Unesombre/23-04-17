#include "LOTL.h"
#include "SessionCharacter.h"
// Date constructor
SessionCharacter::SessionCharacter(int year, int month, int day)
{

}
TSharedRef<IHttpRequest> SessionCharacter::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionCharacter::AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request,int sessionID,int playerID){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "This is the Session ID " + FString::SanitizeFloat(sessionID));
	URL = URL + "session=" + FString::SanitizeFloat(sessionID) + "&" + "user=" + FString::SanitizeFloat(playerID);
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionCharacter::ChooseFaction(FString faction, float playerID, float sessionID){
	//Create Object to send 
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	RefreshIP();
	//JsonObject->SetStringField(TEXT("Session_name"), SessionName);
	JsonObject->SetStringField(TEXT("User_Acc_Id"), FString::FromInt(playerID));
	JsonObject->SetStringField(TEXT("Session_id"), FString::FromInt(sessionID));
	JsonObject->SetStringField(TEXT("Faction"),faction);
	//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, FString::FromInt(playerID) + " " + FString::FromInt(sessionID) + " " + faction);

	return(CreatePostSend(JsonObject, URL));
}
void SessionCharacter::RefreshIP(){
	URL = IP + EXTENSION;
}
