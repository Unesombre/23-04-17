// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "HttpActor.generated.h"



UCLASS()
class LOTL_API AHttpActor : public AActor
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	//Create Unreal engine settup, and http module for requests to the server 
	FHttpModule* Http;
	AHttpActor(const class FObjectInitializer& ObjectInitializer);
	//Used to remove extra json data to make returned data usfull
	FString RemoveNestedJsonComponents(FHttpResponsePtr Response);
	//I love yummy cock in my belly yummy yummy cock "juice 2016"
	void LoginResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void CreateAcountResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void FindGameResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void FindJoinedGameResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void FindTerritoryResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void CreateSessionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void SendFactionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void UnitBuyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void SendChatResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void BuildingBuyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void ChatResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void GetPlayersResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void RefreshResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void EconomyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void PathResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void GetUnitsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	bool ReconfigureIP();
	FString newIP = "";
	float sessionID = 0;
	//Blueprint functions
	UFUNCTION(BlueprintCallable, Category = "Login")
		void LoginAcount(FString UserName, FString Password,FString IP);
	UFUNCTION(BlueprintCallable, Category = "GetEconomy")
		void RefreshEconomy(float PlayerSessionID);
	UFUNCTION(BlueprintCallable, Category = "CreateAcount")
		void CreateAcount(FString FullName, FString UserName, FString Password, FString Email, FString DOB, FString City,FString IP);
	UFUNCTION(BlueprintCallable, Category = "SendBattlePath")
		void CreateUnitPath(FString xArray, FString yArray, FString EndtimeArray, float sessionID, float sessionPlayer, FString UnitID);
	UFUNCTION(BlueprintCallable, Category = "FindGame")
		void FindGames();
	UFUNCTION(BlueprintCallable, Category = "FindJoinedGames")
		void FindJoinedGames(float playerID);
	UFUNCTION(BlueprintCallable, Category = "GetTerritory")
		void GetTerritory();
	UFUNCTION(BlueprintCallable, Category = "SendFaction")
		void SendFaction(FString faction,float playerID,float sessionId);
	UFUNCTION(BlueprintCallable, Category = "GetExsistingGameData")
		void GetExsistingGameData(float sessionId,float playerID,bool Refresh);
	UFUNCTION(BlueprintCallable, Category = "CreateGame")
		void CreateSession(FString ID,FString gameName,FString numberOfPlayers,bool timedGame);
	UFUNCTION(BlueprintCallable, Category = "BuyUnit")
		void CreateUnit(float sessionId, FString UnitType, FString UnitSize, FString UnitOwner, FString X, FString Y);
	UFUNCTION(BlueprintCallable, Category = "SendChat")
		void SendMessage(float sessionId, float playerId, FString Message);
	UFUNCTION(BlueprintCallable, Category = "BuyBuilding")
		void CreateBuilding(float Session_Id, FString Building_Type, FString Building_Owner, FString Building_X, FString Building_Y);
	UFUNCTION(BlueprintCallable, Category = "GetChat")
		void getChat(float sessionId,FString lastCall);
	UFUNCTION(BlueprintCallable, Category = "GetPlayers")
		void GetPlayers(float sessionId);
	UFUNCTION(BlueprintCallable, Category = "GetUnits")
		void GetUnits(float sessionId,FString ignoreList);

	//implementables
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Login Complete"))
		void ServerResponseToBlueprintLogin(const FString& UserID);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Account Created"))
		void ServerResponseToBlueprintAccountCreation(const FString& UserID);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Find Games"))
		void ServerResponseToBlueprintFindGames(const FString& GameName);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "FindMessages"))
		void ServerResponseToBlueprintMessages(const FString& Messages);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Games Found"))
		void ServerResponseToBlueprintFindGamesCompleted();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Messages Found"))
		void ServerResponseToBlueprintFindMessagesCompleted();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Joined Games Found"))
		void ServerResponseToBlueprintFindJoinedGamesCompleted();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Created Game"))
		void ServerResponseToBlueprintCreateGame(float GameID);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "JoinGame"))
		void JoiningGame();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GetMap"))
		void LoadMap(const FString& MapData);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GetMapSize"))
		void LoadMapSize(const FString& MapSizeX, const FString& MapSizeY);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GameRefresh"))
		void RefreshMap();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GetTerritory"))
		void LoadTerritory(const FString& TerritoryData);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GetResource"))
		void LoadResource(const FString& ResourceData);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GetUnits"))
		void LoadUnits(const FString& ResourceData);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "UnitBrought"))
		void AddUnit(const FString& ID);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "ChatSent"))
		void AddChat();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "BuildingBrought"))
		void AddBuilding();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GotPlayers"))
		void AddPlayers(const FString& UserNames, const FString& PlayerIDs);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "RefreshPlayers"))
		void RefreshPlayers();
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "PlayersSessionID"))
		void GetPlayerSessionID(const FString& ID);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "RefeshedEconomy"))
		void GetPlayerEconomy(const FString& Gold, const FString& BuildingResources, const FString& Food);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "StartTime"))
		void GetGameStartTime(const FDateTime& time);
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "GotUnits"))
		void GotUnits(const TArray<FDateTime>& Endtimes, const TArray<FString>& Xs, const TArray<FString>& Ys, const FString& UnitID, const FString& Attack, const FString& Size, const FString& UnitOwner);
	//ERRORS 
	//CODE 
	//1 = LOGIN FALIURE
	//2 = ACCOUNT CREATION FAILURE
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "Login Error"))
		void ServerResponseToBlueprintLoginERROR(const FString& ERRORCODE);

};

