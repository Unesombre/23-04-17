#ifndef Building_H
#define Building_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class BuildingController :public Message
{
private:
	FString URL = Message::IP + "/building_buy/?";
	FString EXTENSION = "/building_buy/?";
	FHttpModule* Http;
public:
	BuildingController(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> CreateBuilding(float Session_Id, FString Building_Type, FString Building_Owner, FString Building_X, FString Building_Y);
	void RefreshIP();
	~BuildingController();
};
#endif