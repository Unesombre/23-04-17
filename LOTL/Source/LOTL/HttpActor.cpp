// Fill out your copyright notice in the Description page of Project Settings.
#include "LOTL.h"
#include "HttpActor.h"
#include "Login.h"
#include "SessionManager.h"
#include "SessionCharacter.h"
#include "GameList.h"
#include "UnitController.h"
#include "ChatController.h"
#include "BuildingController.h"
#include "SessionCharacterGroup.h"
#include "SessionCharacterEconomy.h"
#include "SessionUnitPath.h"

using namespace std;

// Sets default values
AHttpActor::AHttpActor(const class FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	//When the object is constructed, Get the HTTP module
	Http = &FHttpModule::Get();
}
// Called when the game starts or when spawned
void AHttpActor::BeginPlay()
{
	Super::BeginPlay();
}
void AHttpActor::CreateAcountResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "Creation failed");
		ServerResponseToBlueprintLoginERROR("2");
	}

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedLoginID = JsonObject->GetStringField("User_Acc_Id");
		ServerResponseToBlueprintAccountCreation(recievedLoginID);
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
	}

}
void AHttpActor::CreateAcount(FString Fullname, FString UserName, FString Password, FString Email, FString DOB, FString City, FString IP){
	//Create Object to send
	if (IP != ""){
		newIP = "http://" + IP;
	}
	Login login(2,2,2);
	if (ReconfigureIP()){
		login.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> HttpRequest = login.CreateAcount(Fullname, UserName, Password, Email, DOB, City);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::CreateAcountResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();
}
void AHttpActor::LoginAcount(FString UserName, FString Password,FString IP)
{
		if (IP != ""){
			newIP = "http://" + IP;
		}
		Login login(2, 2, 2);
		if (ReconfigureIP()){
			login.SetNewIp(newIP);
		}
		TSharedRef<IHttpRequest> Request = Http->CreateRequest();
		FString parameters[2] = { UserName, Password };
		//set up function to deal with response
		Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::LoginResponse);
		Request = login.AddLoginRequestContent(Request, parameters, sizeof(parameters));
		login.AddGetHeaders(Request);
}
void AHttpActor::LoginResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	
		//Create a pointer to hold the json serialized data
		TSharedPtr<FJsonObject> JsonObject;
		FString data = "";
		if (bWasSuccessful){
			data = RemoveNestedJsonComponents(Response);
		}
		else{
			GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
			ServerResponseToBlueprintLoginERROR("1");
		}

		//Create a reader pointer to read the json data
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedLoginID = JsonObject->GetStringField("User_Acc_Id");
			ServerResponseToBlueprintLogin(recievedLoginID);
			GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
		}

}
void AHttpActor::FindGames(){
	Session sesh(2, 2, 2);
	if (ReconfigureIP()){
		sesh.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::FindGameResponse);
	Request = sesh.AddSessionRequestContent(Request);
	sesh.AddGetHeaders(Request);
}
void AHttpActor::FindJoinedGames(float playerID){
	GameList list(2, 2, 2);
	if (ReconfigureIP()){
		list.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::FindJoinedGameResponse);
	Request = list.AddGamesRequestContent(Request, playerID);
	list.AddGetHeaders(Request);
}
void AHttpActor::FindGameResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	
	FString resultLeft;
	FString resultRight;
	//need to isolate the json data in order to deal with data seperatly
	FString vrtxSpaces = "},{";
	FString jsonData ="";

	while (data.Contains(vrtxSpaces)){
		//if data contains multiple json entries loop untill all data has been removed;
		bool val = data.Split(vrtxSpaces, &resultLeft, &resultRight, ESearchCase::CaseSensitive, ESearchDir::FromStart);
		resultRight = "{"+resultRight;
		resultLeft = resultLeft.Append("}");
		data = resultRight;
		

		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(resultLeft);
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedGameName = JsonObject->GetStringField("Session_name");
			FString recievedGameOwner = JsonObject->GetStringField("Session_creator");
			FString recievedGameNumPlayer = JsonObject->GetStringField("num_of_players");
			FString recievedGameTimedGame = JsonObject->GetStringField("timed_game");
			FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
			FString resultData = recievedGameName + "," + recievedGameOwner + "," + recievedGameNumPlayer + "," + recievedGameTimedGame + "," + recievedGameSessionId;
			ServerResponseToBlueprintFindGames(resultData);
		
		}
	}
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedGameName = JsonObject->GetStringField("Session_name");
		FString recievedGameOwner = JsonObject->GetStringField("Session_creator");
		FString recievedGameNumPlayer = JsonObject->GetStringField("num_of_players");
		FString recievedGameTimedGame = JsonObject->GetStringField("timed_game");
		FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
		FString resultData = recievedGameName + "," + recievedGameOwner + "," + recievedGameNumPlayer + "," + recievedGameTimedGame+"," + recievedGameSessionId;
		ServerResponseToBlueprintFindGames(resultData);
		
		
	}
	//all games have been found
	ServerResponseToBlueprintFindGamesCompleted();

}
void AHttpActor::FindJoinedGameResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);

	FString resultLeft;
	FString resultRight;
	//need to isolate the json data in order to deal with data seperatly
	FString vrtxSpaces = "},{";
	FString jsonData = "";

	while (data.Contains(vrtxSpaces)){
		//if data contains multiple json entries loop untill all data has been removed;
		bool val = data.Split(vrtxSpaces, &resultLeft, &resultRight, ESearchCase::CaseSensitive, ESearchDir::FromStart);
		resultRight = "{" + resultRight;
		resultLeft = resultLeft.Append("}");
		data = resultRight;


		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(resultLeft);
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedGameName = JsonObject->GetStringField("Session_name");
			FString recievedGameOwner = JsonObject->GetStringField("Session_creator");
			FString recievedGameNumPlayer = JsonObject->GetStringField("num_of_players");
			FString recievedGameTimedGame = JsonObject->GetStringField("timed_game");
			FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
			FString resultData = recievedGameName + "," + recievedGameOwner + "," + recievedGameNumPlayer + "," + recievedGameTimedGame + "," + recievedGameSessionId;
			GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, resultData);
			ServerResponseToBlueprintFindGames(resultData);

		}
	}
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedGameName = JsonObject->GetStringField("Session_name");
		FString recievedGameOwner = JsonObject->GetStringField("Session_creator");
		FString recievedGameNumPlayer = JsonObject->GetStringField("num_of_players");
		FString recievedGameTimedGame = JsonObject->GetStringField("timed_game");
		FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
		FString resultData = recievedGameName + "," + recievedGameOwner + "," + recievedGameNumPlayer + "," + recievedGameTimedGame + "," + recievedGameSessionId;
		
		ServerResponseToBlueprintFindGames(resultData);


	}
	//all games have been found
	ServerResponseToBlueprintFindJoinedGamesCompleted();

}
void AHttpActor::CreateSession(FString ID,FString gameName,FString numberOfPlayers,bool timedGame){
	//Create Object to send 
	Session sesh(2, 2, 2);
	if (ReconfigureIP()){
		sesh.SetNewIp(newIP);
	}
	int timedGameVal;
	if (timedGame){
		timedGameVal = 1;
	}
	else{
		timedGameVal = 0;
	}
	TSharedRef<IHttpRequest> HttpRequest = sesh.CreateSession(gameName, FCString::Atoi(*ID), FCString::Atoi(*numberOfPlayers), timedGameVal);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::CreateSessionResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();
}
void AHttpActor::CreateSessionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, data);
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "ping");
		//Get the value of the json object by field name
		FString recievedLoginID = JsonObject->GetStringField("Session_id");
		float MyShinyNewFloat = FCString::Atof(*recievedLoginID);
		ServerResponseToBlueprintCreateGame(MyShinyNewFloat);
		//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedLoginID);
		
	}
}
void AHttpActor::SendFaction(FString faction, float playerID,float sessionId){

	SessionCharacter  character(1, 1, 1);
	if (ReconfigureIP()){
		character.SetNewIp(newIP);
	}
	this->sessionID = sessionId;
	TSharedRef<IHttpRequest> HttpRequest = character.ChooseFaction(faction, playerID, sessionID);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::SendFactionResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();

}
void AHttpActor::SendFactionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	JoiningGame();
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedMapGenRaw = JsonObject->GetStringField("map_gen");
		FString recievedTerritoryRaw = JsonObject->GetStringField("Territory_map");
		FString recievedResourceRaw = JsonObject->GetStringField("Resource_map");
		FString recievedUnitRaw = JsonObject->GetStringField("Unit_map");
		FString sessionPlayerIDRaw = JsonObject->GetStringField("Session_player");
		FString recievedTimeRaw = JsonObject->GetStringField("Time_start");
		FString unwantedBit = TEXT("[");
		FString unwantedBit2 = TEXT("]");
		recievedMapGenRaw.RemoveFromStart(unwantedBit);
		recievedMapGenRaw.RemoveFromEnd(unwantedBit2);
		recievedTerritoryRaw.RemoveFromStart(unwantedBit);
		recievedTerritoryRaw.RemoveFromEnd(unwantedBit2);
		recievedResourceRaw.RemoveFromStart(unwantedBit);
		recievedResourceRaw.RemoveFromEnd(unwantedBit2);
		recievedUnitRaw.RemoveFromStart(unwantedBit);
		recievedUnitRaw.RemoveFromEnd(unwantedBit2);
		sessionPlayerIDRaw.RemoveFromStart(unwantedBit);
		sessionPlayerIDRaw.RemoveFromEnd(unwantedBit2);
		recievedTimeRaw.RemoveFromStart(unwantedBit);
		recievedTimeRaw.RemoveFromEnd(unwantedBit2);
		FString recievedMapRows = JsonObject->GetStringField("map_row");
		FString recievedMapCols = JsonObject->GetStringField("map_col");
		LoadMapSize(recievedMapRows,recievedMapCols);
		GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedTerritoryRaw);
		LoadMap(recievedMapGenRaw);
		LoadTerritory(recievedTerritoryRaw);
		LoadResource(recievedResourceRaw);
		LoadUnits(recievedUnitRaw);
		GetPlayerSessionID(sessionPlayerIDRaw);

		FDateTime Time;
		FDateTime::Parse(recievedTimeRaw, Time);
		FDateTime* pointer = &Time;
		FString timeString = Time.ToString();
		GetGameStartTime(Time);

		//not ready yet
		//GetTerritory();
	}
}
void AHttpActor::GetExsistingGameData(float sessionId,float playerID,bool Refresh){
	SessionCharacter  character(1, 1, 1);
	if (ReconfigureIP()){
		character.SetNewIp(newIP);
	}
	this->sessionID = sessionId;

	TSharedRef<IHttpRequest> HttpRequest = Http->CreateRequest();
	if (Refresh){
		HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::RefreshResponse);
	}
	else{
		HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::SendFactionResponse);
	}
	
	HttpRequest = character.AddSessionSpecificRequestContent(HttpRequest, sessionID, playerID);
	//Send request to the server
	character.AddGetHeaders(HttpRequest);
	//HttpRequest->ProcessRequest();
}
void AHttpActor::GetTerritory(){
	SessionCharacter sesh(2, 2, 2);
	if (ReconfigureIP()){
		sesh.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::FindTerritoryResponse);
	Request = sesh.AddSessionRequestContent(Request);
	sesh.AddGetHeaders(Request);
}
void AHttpActor::FindTerritoryResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);

	FString resultLeft;
	FString resultRight;
	//need to isolate the json data in order to deal with data seperatly
	FString vrtxSpaces = "},{";
	FString jsonData = "";

	while (data.Contains(vrtxSpaces)){
		//if data contains multiple json entries loop untill all data has been removed;
		bool val = data.Split(vrtxSpaces, &resultLeft, &resultRight, ESearchCase::CaseSensitive, ESearchDir::FromStart);
		resultRight = "{" + resultRight;
		resultLeft = resultLeft.Append("}");
		data = resultRight;
		//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Red, "Looking for game files");

		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(resultLeft);
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{		
			//Get the value of the json object by field name
			FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
			float gameID = FCString::Atof(*recievedGameSessionId);
			GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Red, FString::SanitizeFloat(sessionID) + " and " + FString::SanitizeFloat(gameID));
			if (sessionID == gameID){
				GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "Game sesh found");
			}

		}
	}
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedGameSessionId = JsonObject->GetStringField("Session_id");
		float gameID = FCString::Atof(*recievedGameSessionId);
		if (sessionID == gameID){
			GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "Game sesh found");
		}


	}
}
void AHttpActor::CreateUnit(float sessionId, FString UnitType, FString UnitSize, FString UnitOwner, FString X, FString Y) {

	UnitController  NewUnit(1,1,1);
	if (ReconfigureIP()){
		NewUnit.SetNewIp(newIP);
	}
	this->sessionID = sessionId;
	TSharedRef<IHttpRequest> HttpRequest = NewUnit.CreateUnit(sessionID, UnitType, UnitSize, UnitOwner, X, Y);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::UnitBuyResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();

}
void AHttpActor::SendMessage(float sessionId, float playerId, FString Message) {

	ChatController chat(2, 2, 2);
	if (ReconfigureIP()){
		chat.SetNewIp(newIP);
	}
	this->sessionID = sessionId;
	TSharedRef<IHttpRequest> HttpRequest = chat.CreateChat(sessionId, playerId, Message);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::SendChatResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();

}
void AHttpActor::SendChatResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
		ServerResponseToBlueprintLoginERROR("1");
	}

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedLoginID = JsonObject->GetStringField("User_Acc_Id");
		AddChat();
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
	}

}
void AHttpActor::UnitBuyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
		ServerResponseToBlueprintLoginERROR("1");
	}

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedLoginID = JsonObject->GetStringField("User_Acc_Id");
		FString recievedUnitID = JsonObject->GetStringField("Unit_Id");
		AddUnit(recievedUnitID);
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
	}

}
void AHttpActor::CreateBuilding(float Session_Id, FString Building_Type, FString Building_Owner, FString Building_X, FString Building_Y) {

	BuildingController  NewUnit(1, 1, 1);
	if (ReconfigureIP()){
		NewUnit.SetNewIp(newIP);
	}
	this->sessionID = Session_Id;
	TSharedRef<IHttpRequest> HttpRequest = NewUnit.CreateBuilding(Session_Id, Building_Type, Building_Owner, Building_X, Building_Y);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::BuildingBuyResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();

}
void AHttpActor::BuildingBuyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
		ServerResponseToBlueprintLoginERROR("1");
	}

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedLoginID = JsonObject->GetStringField("User_Acc_Id");
		AddBuilding();
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
	}

}
void AHttpActor::getChat(float sessionId,FString lastCall){
	ChatController chat(2, 2, 2);
	if (ReconfigureIP()){
		chat.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::ChatResponse);
	Request = chat.AddSessionChatRequestContent(Request, sessionId,lastCall);
	chat.AddGetHeaders(Request);
}
void AHttpActor::ChatResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);

	FString resultLeft;
	FString resultRight;
	//need to isolate the json data in order to deal with data seperatly
	FString vrtxSpaces = "},{";
	FString jsonData = "";

	while (data.Contains(vrtxSpaces)){
		//if data contains multiple json entries loop untill all data has been removed;
		bool val = data.Split(vrtxSpaces, &resultLeft, &resultRight, ESearchCase::CaseSensitive, ESearchDir::FromStart);
		resultRight = "{" + resultRight;
		resultLeft = resultLeft.Append("}");
		data = resultRight;


		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(resultLeft);
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedSenderID = JsonObject->GetStringField("Sender_id");
			FString recievedMessage= JsonObject->GetStringField("Message");
			//FString resultData = 
			ServerResponseToBlueprintMessages(recievedSenderID + ":" + recievedMessage);

		}
	}
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedSenderID = JsonObject->GetStringField("Sender_id");
		FString recievedMessage = JsonObject->GetStringField("Message");
		//FString resultData 
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, recievedMessage);
		ServerResponseToBlueprintMessages(recievedSenderID + ":" + recievedMessage);


	}
	//all games have been found
	ServerResponseToBlueprintFindMessagesCompleted();
}
void AHttpActor::GetPlayers(float sessionId){
	SessionCharacterGroup players(2, 2, 2);
	if (ReconfigureIP()){
		players.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::GetPlayersResponse);
	Request = players.AddSessionSpecificRequestContent(Request,sessionID);
	players.AddGetHeaders(Request);
}
void AHttpActor::GetPlayersResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
		ServerResponseToBlueprintLoginERROR("1");
	}
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedUserNames = JsonObject->GetStringField("User_name");
		FString recievedUserIDs = JsonObject->GetStringField("Session_player_id");
		//ServerResponseToBlueprintLogin(recievedLoginID);
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
		AddPlayers(recievedUserNames, recievedUserIDs);
	}

}
void AHttpActor::RefreshResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	RefreshPlayers();
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedTerritoryRaw = JsonObject->GetStringField("Territory_map");
		FString recievedResourceRaw = JsonObject->GetStringField("Resource_map");
		FString recievedUnitRaw = JsonObject->GetStringField("Unit_map");
		FString recievedTimeRaw = JsonObject->GetStringField("Time_start");
		FString unwantedBit = TEXT("[");
		FString unwantedBit2 = TEXT("]");


		recievedTerritoryRaw.RemoveFromStart(unwantedBit);
		recievedTerritoryRaw.RemoveFromEnd(unwantedBit2);
		recievedResourceRaw.RemoveFromStart(unwantedBit);
		recievedResourceRaw.RemoveFromEnd(unwantedBit2);
		recievedUnitRaw.RemoveFromStart(unwantedBit);
		recievedUnitRaw.RemoveFromEnd(unwantedBit2);
		recievedTimeRaw.RemoveFromStart(unwantedBit);
		recievedTimeRaw.RemoveFromEnd(unwantedBit2);
		FString recievedMapRows = JsonObject->GetStringField("map_row");
		FString recievedMapCols = JsonObject->GetStringField("map_col");
		LoadMapSize(recievedMapRows, recievedMapCols);
		FDateTime Time;
		FDateTime::Parse(recievedTimeRaw, Time);
		FDateTime* pointer = &Time;
		FString timeString = Time.ToString();
		
		GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, timeString);
		GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedTimeRaw);

		LoadTerritory(recievedTerritoryRaw);
		LoadResource(recievedResourceRaw);
		LoadUnits(recievedUnitRaw);
		GetGameStartTime(Time);
		RefreshMap();

		//not ready yet
		//GetTerritory();
	}
}
void AHttpActor::RefreshEconomy(float PlayerSessionID){
	SessionCharacterEcon econ(2, 2, 2);
	if (ReconfigureIP()){
		econ.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::EconomyResponse);
	Request = econ.AddSessionSpecificRequestContent(Request, PlayerSessionID);
	econ.AddGetHeaders(Request);
}
void AHttpActor::EconomyResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, data);
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedGoldRaw = JsonObject->GetStringField("Gold");
		FString recievedBuildResourcesRaw = JsonObject->GetStringField("Building_resources");
		FString recievedFoodRaw = JsonObject->GetStringField("Food");
		FString unwantedBit = TEXT("[");
		FString unwantedBit2 = TEXT("]");

		recievedGoldRaw.RemoveFromStart(unwantedBit);
		recievedGoldRaw.RemoveFromEnd(unwantedBit2);
		recievedBuildResourcesRaw.RemoveFromStart(unwantedBit);
		recievedBuildResourcesRaw.RemoveFromEnd(unwantedBit2);
		recievedFoodRaw.RemoveFromStart(unwantedBit);
		recievedFoodRaw.RemoveFromEnd(unwantedBit2);
		//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedTerritoryRaw);
		GetPlayerEconomy(recievedGoldRaw, recievedBuildResourcesRaw, recievedFoodRaw);
		

		//not ready yet
		//GetTerritory();
	}
}
void AHttpActor::CreateUnitPath(FString X, FString Y, FString Endtime, float sessionID, float sessionPlayer, FString UnitID){
	SessionUnitPath Path(2, 2, 2);
	if (ReconfigureIP()){
		Path.SetNewIp(newIP);
	}
	this->sessionID = sessionID;
	TSharedRef<IHttpRequest> HttpRequest = Path.CreatePath(X, Y, Endtime, sessionID, sessionPlayer, UnitID);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &AHttpActor::PathResponse);
	//Send request to the server
	HttpRequest->ProcessRequest();
}
void AHttpActor::PathResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = RemoveNestedJsonComponents(Response);
	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString recievedGoldRaw = JsonObject->GetStringField("Gold");
		FString recievedBuildResourcesRaw = JsonObject->GetStringField("Building_resources");
		FString recievedFoodRaw = JsonObject->GetStringField("Food");
		FString unwantedBit = TEXT("[");
		FString unwantedBit2 = TEXT("]");

		recievedGoldRaw.RemoveFromStart(unwantedBit);
		recievedGoldRaw.RemoveFromEnd(unwantedBit2);
		recievedBuildResourcesRaw.RemoveFromStart(unwantedBit);
		recievedBuildResourcesRaw.RemoveFromEnd(unwantedBit2);
		recievedFoodRaw.RemoveFromStart(unwantedBit);
		recievedFoodRaw.RemoveFromEnd(unwantedBit2);
	}
}
void AHttpActor::GetUnits(float sessionId,FString ignoreList){
	SessionUnitPath Units(2, 2, 2);
	if (ReconfigureIP()){
		Units.SetNewIp(newIP);
	}
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	//set up function to deal with response
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpActor::GetUnitsResponse);
	Request = Units.AddSessionSpecificRequestContent(Request, sessionID,ignoreList);
	Units.AddGetHeaders(Request);
}
void AHttpActor::GetUnitsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful){
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;
	FString data = "";
	if (bWasSuccessful){
		data = RemoveNestedJsonComponents(Response);
	}
	else{
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "INVALID IP");
		ServerResponseToBlueprintLoginERROR("1");
	}

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(data);
	//Deserialize the json data given Reader and the actual object to deserialize
	//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, data);
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "Ping");
		//Get the value of the json object by field name
		FString recievedUnitIds= JsonObject->GetStringField("UnitsIds");
		FString recievedUnitNames= JsonObject->GetStringField("UnitNames");
		FString recievedUnitAttack = JsonObject->GetStringField("UnitAttack");
		FString recievedUnitSizes = JsonObject->GetStringField("UnitSize");
		FString recievedUnitOwners = JsonObject->GetStringField("UnitOwners");
		FString recievedUnitX = JsonObject->GetStringField("UnitX");
		FString recievedUnitY= JsonObject->GetStringField("UnitY");
		FString UnitEndTimes = JsonObject->GetStringField("UnitEndTimes");

		TArray<FString> UnitsTimesArray = {};
		TArray<FString> UnitsXsArray = {};
		TArray<FString> UnitsYsArray = {};
		TArray<FString> UnitAttacksArray = {};
		TArray<FString> UnitSizesArray = {};
		TArray<FString> UnitsIDsArray = {}; 
		TArray<FString> UnitsOwners = {};
		UnitEndTimes.ParseIntoArray(UnitsTimesArray, TEXT("#"), false);
		recievedUnitX.ParseIntoArray(UnitsXsArray, TEXT("#"), false);
		recievedUnitY.ParseIntoArray(UnitsYsArray, TEXT("#"), false);
		recievedUnitAttack.ParseIntoArray(UnitAttacksArray, TEXT(","), false);
		recievedUnitSizes.ParseIntoArray(UnitSizesArray, TEXT(","), false);
		recievedUnitIds.ParseIntoArray(UnitsIDsArray, TEXT(","), false);
		recievedUnitOwners.ParseIntoArray(UnitsOwners, TEXT(","), false);
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, recievedUnitX);
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, UnitsXsArray[1]);
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, recievedUnitIds);
		for (int counter = 0; counter < UnitsTimesArray.Num()-1; counter++){
			//int val = UnitsIDsArray.Num();
			int val = UnitsTimesArray.Num();
			//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, FString::FromInt(val));
			//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, recievedUnitX);
			if (counter > 0){
				FString unwantedBit = TEXT(",");
				UnitsXsArray[counter].RemoveFromStart(unwantedBit);
				UnitsYsArray[counter].RemoveFromStart(unwantedBit);
			}
			TArray<FString> UnitTimesArray = {};
			TArray<FString> UnitXsArray = {};
			TArray<FString> UnitYsArray = {};
			TArray<FDateTime> UnitFDateTimes = {};
			UnitsTimesArray[counter].ParseIntoArray(UnitTimesArray, TEXT(","), false);
			UnitsXsArray[counter].ParseIntoArray(UnitXsArray, TEXT(","), false);
			UnitsYsArray[counter].ParseIntoArray(UnitYsArray, TEXT(","), false);
			for (int dateCounter = 0; dateCounter < UnitTimesArray.Num(); dateCounter++){
				FDateTime Time;
				FDateTime::Parse(UnitTimesArray[dateCounter], Time);
				UnitFDateTimes.Add(Time);
			}
			///or each
			if (UnitXsArray.Num()>0){
				//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, UnitXsArray[0] + ":" + UnitYsArray[0]);
				//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, recievedUnitIds);
				GotUnits(UnitFDateTimes, UnitXsArray, UnitYsArray, UnitsIDsArray[counter], UnitAttacksArray[counter], UnitSizesArray[counter], UnitsOwners[counter]);
			}
		}




		//ServerResponseToBlueprintLogin(recievedLoginID);
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, "ID IS " + recievedLoginID);
		//AddPlayers(recievedUserNames, recievedUserIDs);
	}

}
FString AHttpActor::RemoveNestedJsonComponents(FHttpResponsePtr Response){
	FString data;
		data = Response->GetContentAsString();
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, data);
		FString unwantedBit = TEXT("[");
		FString unwantedBit2 = TEXT("]");
		data.RemoveFromStart(unwantedBit);
		data.Replace(TEXT("\\"), TEXT(""));
		data.RemoveFromEnd(unwantedBit2);
	
	return data;
}
bool AHttpActor::ReconfigureIP(){
	if (newIP!= ""){
		return true;
	}
	else{
		return false;
	}
}


