#include "LOTL.h"
#include "UnitController.h"


UnitController::UnitController(int year, int month, int day)
{

}

TSharedRef<IHttpRequest> UnitController::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//String serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> UnitController::CreateUnit(float sessionID, FString UnitType, FString UnitSize, FString UnitOwner, FString X, FString Y){
	//Create Object to send 
	RefreshIP();
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());

	JsonObject->SetStringField(TEXT("Session_Id"), FString::FromInt(sessionID));
	JsonObject->SetStringField(TEXT("Unit_Type"), UnitType);
	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "unit type is " + UnitType);
	JsonObject->SetStringField(TEXT("Unit_Size"), UnitSize);
	JsonObject->SetStringField(TEXT("Unit_Owner"), UnitOwner);
	JsonObject->SetStringField(TEXT("Unit_X"), X);
	JsonObject->SetStringField(TEXT("Unit_Y"), Y);
	return(CreatePostSend(JsonObject, URL));
}
void UnitController::RefreshIP(){
	URL = IP + EXTENSION;
}

UnitController::~UnitController()
{
}
