
#ifndef SESSION_H
#define SESSION_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class Session : public Message
{
private:
	FString URL = Message::IP + "/session/?";
	FString EXTENSION = "/session/?";
	FHttpModule* Http;
public:
	Session(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> CreateSession(FString SessionName, int SessionCreator, int NumOfPlayers,int TimedGame);
	void RefreshIP();
};

#endif