#ifndef GameList_H
#define GameList_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class GameList : public Message
{
private:
	FString URL = Message::IP + "/sessions-joined/?";
	FString EXTENSION = "/sessions-joined/?";
	FHttpModule* Http;
public:
	GameList(int year, int month, int day);
	TSharedRef<IHttpRequest> AddGamesRequestContent(TSharedRef<IHttpRequest> Request, float playerID);
	void RefreshIP();
};

#endif