
#ifndef LOGIN_H
#define LOGIN_H
#include "Message.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
class Login : public Message
{
private:
	FString URL = Message::IP+"/user/?";  
	FString EXTENSION = "/user/?";
	FHttpModule* Http;
public:
	Login(int year, int month, int day);
	TSharedRef<IHttpRequest> AddLoginRequestContent(TSharedRef<IHttpRequest> Request, FString parameters[],int numOfParams);
	TSharedRef<IHttpRequest> CreateAcount(FString FullName, FString UserName, FString Password, FString Email, FString DOB, FString City);
	void RefreshIP();
};

#endif