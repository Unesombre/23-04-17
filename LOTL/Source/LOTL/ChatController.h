#ifndef CHAT_H
#define CHAT_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class ChatController :public Message
{
private:
	FString URL = Message::IP + "/session_chat/?";
	FString EXTENSION = "/session_chat/?";
	FHttpModule* Http;
public:
	ChatController(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> CreateChat(float sessionID, float playerID, FString Message);
	TSharedRef<IHttpRequest> AddSessionChatRequestContent(TSharedRef<IHttpRequest> Request, float sessionID, FString lastCall);
	void RefreshIP();
	~ChatController();
};
#endif