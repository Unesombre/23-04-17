#include "LOTL.h"
#include "SessionCharacterEconomy.h"
// Date constructor
SessionCharacterEcon::SessionCharacterEcon(int year, int month, int day)
{

}
TSharedRef<IHttpRequest> SessionCharacterEcon::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionCharacterEcon::AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request, int PlayerSessionID){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "This is the Session ID " + FString::SanitizeFloat(sessionID));
	URL = URL + "Session_player=" + FString::SanitizeFloat(PlayerSessionID);
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionCharacterEcon::ChooseFaction(FString faction, float playerID, float sessionID){
	//Create Object to send 
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	RefreshIP();
	//JsonObject->SetStringField(TEXT("Session_name"), SessionName);
	JsonObject->SetStringField(TEXT("User_Acc_Id"), FString::FromInt(playerID));
	JsonObject->SetStringField(TEXT("Session_id"), FString::FromInt(sessionID));
	JsonObject->SetStringField(TEXT("Faction"),faction);
	//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, FString::FromInt(playerID) + " " + FString::FromInt(sessionID) + " " + faction);

	return(CreatePostSend(JsonObject, URL));
}
void SessionCharacterEcon::RefreshIP(){
	URL = IP + EXTENSION;
}
