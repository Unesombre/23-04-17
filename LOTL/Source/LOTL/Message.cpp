#include "LOTL.h"
#include "Message.h"


Message::Message()
{

}
TSharedRef<IHttpRequest> Message::CreatePostSend(TSharedPtr<FJsonObject> JsonObject,FString URL)
{
	//URL
	FString OutputString;

	TSharedRef<TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&OutputString);

	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), JsonWriter);

	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	HttpRequest->SetVerb("POST");

	HttpRequest->SetHeader("Content-Type", "application/json");

	HttpRequest->SetURL(*FString::Printf(TEXT("%s"), *URL));

	HttpRequest->SetContentAsString(OutputString);

	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, OutputString);

	return HttpRequest;
}
void Message::AddGetHeaders(TSharedRef<IHttpRequest> Request)
{
	//Adds basic get headers used for all get requests
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	//Send get request

	Request->ProcessRequest();

}
void Message::SetNewIp(FString newIp)
{
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, IP);
	this->IP = newIp;
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, newIp);
}
Message::~Message()
{

}
