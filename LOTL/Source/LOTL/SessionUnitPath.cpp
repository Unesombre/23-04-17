#include "LOTL.h"
#include "SessionUnitPath.h"
// Date constructor
SessionUnitPath::SessionUnitPath(int year, int month, int day)
{

}
TSharedRef<IHttpRequest> SessionUnitPath::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionUnitPath::AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request,float sessionID,FString ignoreList){
	//FString serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "This is the unit List ID " + ignoreList);
	URL = URL + "session=" + FString::SanitizeFloat(sessionID) + "&" + "units=" + ignoreList;

	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> SessionUnitPath::CreatePath(FString X, FString Y, FString Endtime, float sessionID, float sessionPlayer, FString UnitID){
	//Create Object to send 
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	RefreshIP();
	//JsonObject->SetStringField(TEXT("Session_name"), SessionName);
	JsonObject->SetStringField(TEXT("Session_id"), FString::FromInt(sessionID));
	JsonObject->SetStringField(TEXT("unit_id"), UnitID);
	JsonObject->SetStringField(TEXT("Unit_X"), X);
	JsonObject->SetStringField(TEXT("Unit_Y"), Y);
	JsonObject->SetStringField(TEXT("end_times"),Endtime);
	GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, UnitID + " ID IDIDIDIDIDID ");

	return(CreatePostSend(JsonObject, URL));
}
void SessionUnitPath::RefreshIP(){
	URL = IP + EXTENSION;
}
