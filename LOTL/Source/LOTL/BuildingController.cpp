#include "LOTL.h"
#include "BuildingController.h"


BuildingController::BuildingController(int year, int month, int day)
{

}

TSharedRef<IHttpRequest> BuildingController::AddSessionRequestContent(TSharedRef<IHttpRequest> Request){
	//String serverLocation = URL;
	RefreshIP();
	//This is the url on which to process the request
	Request->SetURL(URL);
	return Request;
}
TSharedRef<IHttpRequest> BuildingController::CreateBuilding(float Session_Id, FString Building_Type, FString Building_Owner, FString Building_X, FString Building_Y){
	//Create Object to send 
	RefreshIP();
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());

	JsonObject->SetStringField(TEXT("Session_Id"), FString::FromInt(Session_Id));
	JsonObject->SetStringField(TEXT("Building_Type"), Building_Type);
	JsonObject->SetStringField(TEXT("Building_Owner"), Building_Owner);
	JsonObject->SetStringField(TEXT("Building_X"), Building_X);
	JsonObject->SetStringField(TEXT("Building_Y"), Building_Y);
	GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, "sent building stuff");
	return(CreatePostSend(JsonObject, URL));
}
void BuildingController::RefreshIP(){
	URL = IP + EXTENSION;
}

BuildingController::~BuildingController()
{
}
