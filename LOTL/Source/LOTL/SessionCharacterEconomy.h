#ifndef CHARACTER_ECON_H
#define CHARACTER_ECON_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class SessionCharacterEcon : public Message
{
private:
	FString URL = Message::IP + "/user_economy/?";
	FString EXTENSION = "/user_economy/?";
	FHttpModule* Http;
public:
	SessionCharacterEcon(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request, int PlayerSessionID);
	TSharedRef<IHttpRequest> ChooseFaction(FString faction, float playerID, float sessionID);
	TSharedRef<IHttpRequest> GetTerritoryMap(float playerID, float sessionID);
	void RefreshIP();
};

#endif