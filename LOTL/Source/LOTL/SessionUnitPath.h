#ifndef SESSION_PATH_H
#define SESSION_PATH_H
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Message.h"
class SessionUnitPath : public Message
{
private:
	FString URL = Message::IP + "/unit_movement/?";
	FString EXTENSION = "/unit_movement/?";
	FHttpModule* Http;
public:
	SessionUnitPath(int year, int month, int day);
	TSharedRef<IHttpRequest> AddSessionRequestContent(TSharedRef<IHttpRequest> Request);
	TSharedRef<IHttpRequest> AddSessionSpecificRequestContent(TSharedRef<IHttpRequest> Request, float sessionID, FString ignoreList);
	TSharedRef<IHttpRequest> CreatePath(FString X, FString Y, FString Endtime, float sessionID, float sessionPlayer, FString UnitID);
	TSharedRef<IHttpRequest> GetTerritoryMap(float playerID, float sessionID);
	void RefreshIP();
};

#endif